def test_os_version(host):
    assert host.file("/etc/os-release").contains("Alpine")


def test_nginx_installed(host):
    assert host.service("nginx").is_running is False


def test_nginx_version(host):
    assert host.package("nginx").version
